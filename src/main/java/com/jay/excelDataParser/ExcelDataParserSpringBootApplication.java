package com.jay.excelDataParser;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * import excel data into database table by reading XML setting
 * @author Jay (JayTs.Liu@gmail.com)
 * @since 20180305
 * @version 0.1
 */
@SpringBootApplication
public class ExcelDataParserSpringBootApplication implements CommandLineRunner {
	
	@Autowired
	private ExcelDataParser excelDataParser;
	
	public static void main(String[] args) {
		SpringApplication.run(ExcelDataParserSpringBootApplication.class, args);
	}
	
	@Override
	public void run(String... args) throws Exception {

		excelDataParser.exec();
	}
}
