package com.jay.excelDataParser.model;

public enum ReportStatus {
	
	I,	//import source file
	R,	//read rows
	Q,	//prepare query
	DB,	//database insert 
	FDB,//flush data in trunk
	T,	//run query to insert data form temp table to target table
	B,	//backup source file
	E,	//error
	S;  //success

}
