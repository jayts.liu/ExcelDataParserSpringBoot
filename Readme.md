# ExcelDataParser

 This prototype demonstrate of using frameworks to import data from excel into database. The system provides flexibility for user to specify the source from and target to by setting files.

### Prerequisites

 The project was tested under windows environment.

 1. Java 8+
 2. Spring-boot 2
 3. apache.commons 4.1
 4. apache.poi 3.17
 5. mssql-jdbc 6.3.6
 6. [JUtil-0.1.jar](http://gitlab.com/jayts.liu/JUtil)


### Installing

 1. Ensure your project configured and able to query with database.
 2. Place BasePersistence to your project
 3. Install spring-boot、apache.commons、 poi、mssql-jdbc libs into your project lib. 
 ```
    # configure build.gradle, add following into dependencies
    
	compile group: 'com.microsoft.sqlserver', name: 'mssql-jdbc', version: '6.3.6.jre8-preview'
	compile group: 'org.apache.poi', name: 'poi-ooxml', version: '3.17'
	compile group: 'org.apache.commons', name: 'commons-collections4', version: '4.1'

	compile('org.springframework.boot:spring-boot-starter-jdbc')
 ```

 4. Add JUtil-0.1.jar into your lib see.[JUtil-0.1.jar](http://gitlab.com/jayts.liu/JUtil)
 5. configure excelDataParser.properties
 ```
    excelDataParser.import.template.path=template/ #set your template path
    excelDataParser.update.min.row=5000            #set minimum commit row per time 
    excelDataParser.report.path=doc/report/        #set your data source path(excel files)
 ```
 
 6. Setup template file for system to pair with source file and db target table.
 ```
    <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
    <impCfgTask>
     <source>
		<sourceRootPath>import excel path</sourceRootPath>
		<fileName>source file name(Optional)</fileName>
		<skipTitleLength>number to skip title length</skipTitleLength>
		<sourceColumnLength>source column length</sourceColumnLength>
		<sourceTable>temp table name(Optional)</sourceTable>
		<sourceBackupPath>set backup file path(Optional)</sourceBackupPath>
		<removeSourceFile>true/false to remove source file after backup(Optional)</removeSourceFile>
     </source>
     <destination>
		<destinationTable>target table name</destinationTable>
		 <reflect>
			<sequence>sequence data order(begin from 1)</sequence>
			<sourceColumn>from source column name</sourceColumn>
			<destinationColumn>to target column name</destinationColumn>
		 </reflect>
     </destination>
    </impCfgTask>
 ```
 7. now system is ready to run excelDataParser/ExcelDataParserSpringBootApplication.java
 8. check report file under doc/report
 9. Finish.


### Example

 template/sample.xml
 ~~~bash
  <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
  <configTask>
	<source>
		<sourceRootPath>doc/</sourceRootPath>           #system task will scan setup path to check excel files 
		<fileName>importSample.xlsx</fileName>          #allow empty, if set system will only import specified file
		<skipTitleLength>1</skipTitleLength>			  #if there are header please set number rows to skip
		<sourceColumnLength>5</sourceColumnLength>		  #set for system to number of column per row to insert
		<sourceTable>TEMPTABLE</sourceTable>            #if wish to store to template table before store to target table
		<sourceBackupPath>doc/backup</sourceBackupPath> #path to back up source file
		<removeSourceFile>false</removeSourceFile>      #remove source file after insert complete
	</source>
	<destination>
		<destinationTable>CUSTOMERSTEST</destinationTable> #insert into table db.CUSTOMERSTEST
		<reflect>
			<sequence>1</sequence>                          #tag sequence in excel file
			<sourceColumn>ID</sourceColumn>                 #excel tag name
			<destinationColumn>ID</destinationColumn>       #store to database column name
		</reflect>
		<reflect>
			<sequence>2</sequence>
			<sourceColumn>CUS_CODE</sourceColumn>
			<destinationColumn>CUS_CODE</destinationColumn>
		</reflect>
		<reflect>
			<sequence>3</sequence>
			<sourceColumn>CUS_NAME</sourceColumn>
			<destinationColumn>CUS_NAME</destinationColumn>
		</reflect>
		<reflect>
			<sequence>4</sequence>
			<sourceColumn>HADDRESS</sourceColumn>
			<destinationColumn>HADDRESS</destinationColumn>
		</reflect>
		<reflect>
			<sequence>5</sequence>
			<sourceColumn>PRICE</sourceColumn>
			<destinationColumn>PRICE</destinationColumn>
		</reflect>
	</destination>
  </configTask>
 ~~~

 doc/importSample.xlsx
 ~~~bash
  ID	CUS_CODE	CUS_NAME	ADDRESS	PRICE
  1	cus1	aname1	addressaddress1	1111.2
  2	cus2	aname2	addressaddress2	22.22
  3	cus3Update2223			33.32
 ~~~

 ***NOTE:***
  Please read doc\readme.doc for more instructions.


### Built With

* [Spring Framework](https://spring.io) - The framework used
* [GRADLE](https://gradle.org/) - Dependency Management
* [POI](https://poi.apache.org/) - The framework used


### License

 This project is licensed under the Apache License, Version 2.0 License - see the [LICENSE](LICENSE) file for details


