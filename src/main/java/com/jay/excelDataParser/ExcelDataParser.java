package com.jay.excelDataParser;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;

import com.jay.excelDataParser.exception.ExcelDbParserException;
import com.jay.excelDataParser.model.ConfigTask;
import com.jay.excelDataParser.model.Destination;
import com.jay.excelDataParser.model.Report;
import com.jay.excelDataParser.model.ReportTask;
import com.jay.excelDataParser.model.Source;
import com.jay.excelDataParser.service.ServiceBroker;
import com.jay.base.util.common.FileUtil;
import com.jay.base.util.common.FolderUtil;
import com.jay.base.util.common.PropertiesLoder;

/**
 * import excel data into database table by reading XML setting
 * @author Jay (JayTs.Liu@gmail.com)
 * @since 20180305
 * @version 0.1
 */
@Controller
public class ExcelDataParser {
	
	private final static Properties CONFIG = PropertiesLoder.load("res/excelDataParser.properties");
	private final static String TEMP_PATH = CONFIG.getProperty("excelDataParser.import.template.path");
	private final static Integer UPDATE_MIN_ROW = Integer.valueOf(CONFIG.getProperty("excelDataParser.update.min.row")); //update row per time

	@Autowired
	private ServiceBroker serviceBroker;

	private Report report;

	public void exec() throws IOException {
		initPathInfo();
		List<ConfigTask> configTaskList = parseToConfigTask(getXmlConfigList()); //prepare configure task
		processConfigTask(configTaskList); //start analyze and data insert
		getReport().print(); //print process result
	}
	
	public Report getReport() {
		if(report == null)
			this.report = new Report(TEMP_PATH, UPDATE_MIN_ROW);
		return this.report;
	}

	private void processConfigTask(List<ConfigTask> configTaskList) {
		
		for(ConfigTask task : configTaskList) {
				
				String rootPath = task.getSource().getSourceRootPath();
				String sourceFileName = task.getSource().getFileName();
				List<String> excelList = null;
				
				//validate
				if(rootPath == null || rootPath.equals("")) {
					addErrMsg(task.getXmlName()+".sourceRootPath can not be empty! ");
					continue;
				}
				
				//prepare soure file list
				if(sourceFileName != null && !sourceFileName.isEmpty()) {
					excelList = new ArrayList<>();
					excelList.add(rootPath + sourceFileName);
				}
				else {
					excelList = getExcelList(rootPath);
				}
				
				//start parsing
				for(String excel: excelList) {
					try {
						processExcelData(task, excel);
					} catch (Exception e) {
						e.printStackTrace();
						addErrMsg("Config:" + task.getXmlName() + " Source:" +excel + " Error:" + e.getMessage());
					} 
				}

		}//end for
	}
	
	@Transactional
	private boolean processExcelData(ConfigTask task, String excelPath) throws ExcelDbParserException {
		System.out.println("[Start task: "+task.getXmlName() + "]");
		Source source = task.getSource();
		List<Destination> dstLst = task.getDestinationList();
		ReportTask reportTask = new ReportTask(task, excelPath);
		
		FileInputStream sourceFile = null;
		Workbook workbook = null;
		try {
			reportTask.setStatus("I"); //I: Import
			sourceFile = new FileInputStream(new File(excelPath));
			workbook = WorkbookFactory.create(sourceFile);
			//TODO for (int sheetNo = 0; sheetNo < workbook.getNumberOfSheets(); sheetNo++)
	        Sheet sheet = workbook.getSheetAt(0);
	        
	        int skipRow = source.getSkipTitleLength();
            int flowType = (source.getSourceTable().equals("") ? 2 : 1); //1:store to temp table, 2:store to destination
	        int rowCnt=0;//row
	        int trunkCnt = 0;
	        List<String> sqlList = new ArrayList<>();
	        
	        /* process each row */
	        reportTask.setStatus("R");//R:Rows
	        for (Row row : sheet) { 
	        	if(skipRow > 0 && rowCnt < skipRow) { //to skip rows
	        		rowCnt++;
	        		continue;
	        	}
	        	
	            List<List<Object>> dataList = new ArrayList<>();
	            List<Object> objLst = new ArrayList<>();
	            int col=0;//column

	            /* process each cell */
	            for(int cellIdx=0; cellIdx< row.getLastCellNum(); cellIdx++) { 
	            	Cell currentCell = row.getCell(cellIdx, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK );
	            	
	            	/* process each destination (reflect mapping) */
	                for(int dlIdx=0; dlIdx < dstLst.size(); dlIdx++) { 
	                	if(dataList.isEmpty() || dataList.size() < dstLst.size()) {
	                		objLst = new ArrayList<Object>();
	                		dataList.add(dlIdx, objLst);
	                	}
	                	else { 
	                		objLst = dataList.get(dlIdx);
	                	}
	                	
	                	Destination d = dstLst.get(dlIdx);
	                	int procee = d.getProcee();
	                	int seq = d.getReflectList().get(procee).getSequence()-1; //start from 0
		                if(col==seq) {
		                	//add parameter
		                	if (currentCell.getCellTypeEnum() == CellType.BLANK) //set blank data to null
		                	//if (currentCell.getCellType() == Cell.CELL_TYPE_BLANK ) 
		                		objLst.add(null);
			                else if (currentCell.getCellTypeEnum() == CellType.NUMERIC) //set numeric data 
		                	//else if (currentCell.getCellType() == Cell.CELL_TYPE_NUMERIC)
			                	objLst.add(currentCell.getNumericCellValue());
			                else 
			                	objLst.add(currentCell.getStringCellValue()); //set string data
			                
			                dataList.set(dlIdx, objLst);
			                d.setProcee((procee+1 < d.getReflectList().size()) ? ++procee : 0);
			                dstLst.set(dlIdx, d);
		                }
	                }
	                col++;
	            }

	            reportTask.setStatus("Q");//Q:Prepare Query
	            //prepare insert query
	            for(int i=0; i < dstLst.size();i++) {
                	Destination d = dstLst.get(i);
                	String targetTabel = (flowType ==1? source.getSourceTable() : d.getDestinationTable()); 
                	
                	if(d.getColumnList(flowType).length > dataList.get(i).size())//check column length
                		throw new ExcelDbParserException("unexpected column length error (expected:" + d.getColumnList(flowType).length + " , source:" + dataList.get(i).size() +")");
 
                	sqlList.add(getSql(targetTabel, d.getColumnList(flowType), dataList.get(i)));
                	trunkCnt++;
	            }
	            
	            if(trunkCnt >= UPDATE_MIN_ROW) {//check limit exceed 
	            	reportTask.setStatus("DB");//Database
	            	execQuery(sqlList);// do insert
	            	
	            	reportTask.addInsertRowCount(trunkCnt);
	            	trunkCnt=0;
	            }
	            
	            rowCnt++;
	            reportTask.addRowCount();
	        }//end while iterator
	        
	        if(sqlList != null ) { //flush data in trunk
	        	reportTask.setStatus("FDB");//Database
	        	execQuery(sqlList);
	        	reportTask.addInsertRowCount(trunkCnt);
	        	trunkCnt=0;
	        }
	        
	        if(flowType==1) { //run query to insert data form temp table to target table
	        	reportTask.setStatus("T");
	        	doInsertFromTempTable(dstLst, source.getSourceTable());
	        }
	        
	        if(!source.getSourceBackupPath().equals("")) { //backup source file
	        	reportTask.setStatus("B");
	        	doBackupSourceFile(source);
	        	reportTask.setBackSuccess(true);
	        }
	        
	        reportTask.setStatus("S");
	        System.out.println("[End task: "+task.getXmlName() + "]");
	        return true;
		}  catch (Exception e) {
			reportTask.addErrMsg(e.getMessage());
			throw new ExcelDbParserException(e);
		} finally {
			getReport().addReportTask(reportTask); //add task rport
			try { if(sourceFile!=null) sourceFile.close(); } catch (IOException e) {}
			try { if(workbook != null) workbook.close(); } catch (IOException e) {}
		} 
	}
	
	private void doInsertFromTempTable(List<Destination> destinationList, String sourceTable) {
		List<String> sqlList = new ArrayList<>();
		for(Destination d: destinationList)
			sqlList.add(getSqlTempToTarget(sourceTable, d));
			
		execQuery(sqlList);
	}

	private void doBackupSourceFile(Source source) {
		List<String> fileList = new ArrayList<>();
		String sourceFile = source.getSourceRootPath()+ source.getFileName();
		String targetFile = source.getSourceBackupPath();
		boolean removeYn = source.isRemoveSourceFile();
		
		File sFile = new File(sourceFile); 
		if(sFile.isDirectory())
			fileList.addAll(FolderUtil.getTreeFiles(sourceFile, "xlsx", "xls"));
		else
			fileList.add(sFile.getPath());
		
		for(String sf : fileList) {
			try {
				FileUtil.copyFile(sf, targetFile+ sf.substring(sf.lastIndexOf("\\")), removeYn);
			} catch (IOException e) {
				e.printStackTrace();
				addErrMsg(e.getMessage());
			}
		}//end for
	}
	
	
	private String getSql(String tableName, String[] columnList, List<Object> dataList) {
		StringBuilder sql = new StringBuilder();
		sql.append("INSERT INTO ");
		sql.append(tableName);
		sql.append("(");
		sql.append(Arrays.toString(columnList).replaceAll("[\\[\\]]", ""));
		sql.append(") ");
		sql.append("VALUES(");
		sql.append(getParmasToSqlStr(dataList));
		sql.append("); ");
		
		return sql.toString();
	}
	
	private String getSqlTempToTarget(String sourceTable, Destination destination) {
		StringBuilder sql = new StringBuilder();
		sql.append("INSERT INTO ");
		sql.append(destination.getDestinationTable());
		sql.append("(");
		sql.append(Arrays.toString(destination.getSourceColumn()).replaceAll("[\\[\\]]", ""));
		sql.append(") ");
		sql.append("SELECT ");
		sql.append(Arrays.toString(destination.getDestinationColumn()).replaceAll("[\\[\\]]", ""));
		sql.append(" FROM ").append(sourceTable).append("; ");

		return sql.toString();
	}
	
	private List<String> getXmlConfigList() {
		return FolderUtil.getTreeFiles(TEMP_PATH, ".xml", ".Xml", ".XML");
	}

	private List<String> getExcelList(String path) {
		return FolderUtil.getTreeFiles(TEMP_PATH, ".xlsx");
	}
	
	private String getParmasToSqlStr(List<Object> dataList) {
		StringBuilder sb = new StringBuilder();
		for(int i=0; i < dataList.size() ; i++) {
			if(i > 0)
				sb.append(", ");
			
			if(dataList.get(i) == null)
				sb.append("NULL");
			else if(dataList.get(i) instanceof Number)
				sb.append(dataList.get(i));
			else
				sb.append("'").append(String.valueOf(dataList.get(i)).replaceAll("'", "")).append("'");
		}
		return sb.toString();
	}
	
	private int execQuery(List<String> sqlList) {
		return serviceBroker.execQuery(sqlList).length; 
	}
	
	private List<ConfigTask> parseToConfigTask(List<String> xmlList) { //initial configure file
		List<ConfigTask> accurateTaskList = null;
		
		if (xmlList != null && xmlList.size() > 0) {
			accurateTaskList = new ArrayList<ConfigTask>();
			for (String fileStr : xmlList) {
				File file = new File(fileStr);
				JAXBContext jaxbContext;
				try {
					jaxbContext = JAXBContext.newInstance(ConfigTask.class);
				
					Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
					ConfigTask ct = (ConfigTask) jaxbUnmarshaller.unmarshal(file);
					ct.setXmlName(file.getName());
					
					accurateTaskList.add(ct);
				} catch (JAXBException e) {
					Throwable t = (e.getMessage() == null ? e.getLinkedException() : e);
					getReport().addErrMsg(fileStr, t);
					e.printStackTrace();
				}
			}
		}
		
		return accurateTaskList;
	}
	
	private void addErrMsg(String message) {
		getReport().addErrMsg(message);
	}
	
	private void initPathInfo() {
		System.out.println("CONFIG: " + CONFIG);
		System.out.println("TEMP_PATH: " + TEMP_PATH);
		System.out.println("UPDATE_MIN_ROW: " + UPDATE_MIN_ROW);
	}
}

