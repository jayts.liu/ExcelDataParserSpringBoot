package com.jay.excelDataParser.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Jay (JayTs.Liu@gmail.com)
 * @since 20180305
 * @version 0.1
 */
@XmlRootElement
public class ConfigTask {

	private String xmlName;
	private Source source;
	private List<Destination> destinationList;

	public ConfigTask() {
		super();
	}
	
	public Source getSource() {
		return source;
	}

	public void setSource(Source source) {
		this.source = source;
	}
	
	@XmlElement(name = "destination")
	public List<Destination> getDestinationList() {
		return destinationList;
	}

	public void setDestinationList(List<Destination> destinationList) {
		this.destinationList = destinationList;
	}

	public String getXmlName() {
		return xmlName;
	}

	public void setXmlName(String xmlName) {
		this.xmlName = xmlName;
	}
	
	public String getDestinationTableToString() {
		StringBuilder sb = new StringBuilder();
		if (destinationList != null) 
			for(int i=0; i< destinationList.size(); i++) {
				if(i > 0) sb.append(",");
				sb.append(destinationList.get(i).getDestinationTable());
			}
		return sb.toString();
	}

	@Override
	public String toString() {
		return "ImpCfgTask [source=" + source + ", destination=" + getDestinationTableToString() + "]";
	}

}




