package com.jay.excelDataParser.service;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Service;

/**
 * 
 * @author Jay (JayTs.Liu@gmail.com)
 * @since 20180305
 * @version 0.1
 */
@Service
public class ServiceBroker extends JdbcDaoSupport {
	
	@Autowired
	private DataSource dataSource;

	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	public int[] execQuery(List<String> sqlList) { 
		return getJdbcTemplate().batchUpdate(sqlList.toArray(new String[sqlList.size()]));
	}
}
