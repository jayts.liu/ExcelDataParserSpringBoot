package com.jay.excelDataParser.model;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Jay (JayTs.Liu@gmail.com)
 * @since 20180305
 * @version 0.1
 */
public class ReportTask {
	
	private String configName;
	private String sourceFile;
	private String temptable;
	private String targetTables;
	private int rowCount;//input row count excel
	private int insertRowCount; 
	private String status;
	private List<String> errMsgList;
	private boolean backSuccess;
	private String backUpPath;
	
	public ReportTask(ConfigTask task, String sourceFile) {
		this.configName = task.getXmlName();
		this.sourceFile = sourceFile;
		this.temptable = task.getSource().getSourceTable();
		this.targetTables = task.getDestinationTableToString();
		this.backUpPath = task.getSource().getSourceBackupPath();
	}

	public String getConfigName() {
		return configName;
	}

	public void setConfigName(String configName) {
		this.configName = configName;
	}
	
	public String getSourceFile() {
		return sourceFile;
	}

	public void setSourceFile(String sourceFile) {
		this.sourceFile = sourceFile;
	}
	
	public String getTemptable() {
		return temptable;
	}

	public void setTemptable(String temptable) {
		this.temptable = temptable;
	}

	public int getRowCount() {
		return rowCount;
	}

	public void setRowCount(int rowCount) {
		this.rowCount = rowCount;
	}
	
	public void addRowCount() {
		this.rowCount++;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String i) {
		this.status = i;
	}

	public List<String> getErrMsgList() {
		if (this.errMsgList == null)
			this.errMsgList = new ArrayList<>();
		return this.errMsgList;
	}

	public void setErrMsgList(List<String> errMsgList) {
		this.errMsgList = errMsgList;
	}

	public void addErrMsg(String message) {
		this.status = "E" + status;  //begin with E for error type
		getErrMsgList().add(message);
	}

	public String getTargetTables() {
		return targetTables;
	}

	public void setTargetTables(String targetTables) {
		this.targetTables = targetTables;
	}

	public boolean isBackSuccess() {
		return backSuccess;
	}

	public void setBackSuccess(boolean backSuccess) {
		this.backSuccess = backSuccess;
	}

	public String getBackUpPath() {
		return backUpPath;
	}

	public void setBackUpPath(String backUpPath) {
		this.backUpPath = backUpPath;
	}

	public int getInsertRowCount() {
		return insertRowCount;
	}

	public void setInsertRowCount(int insertRowCount) {
		this.insertRowCount = insertRowCount;
	}
	
	public void addInsertRowCount(int numberToAdd) {
		this.insertRowCount += numberToAdd;
	}

}
